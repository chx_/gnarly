<?php

namespace Drupal\gnarly\Path;

use Drupal\Core\Language\LanguageDefault;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Path\AliasStorageInterface;

class GnarlyAliasStorage implements AliasStorageInterface {

  /**
   * @var \Drupal\Core\Path\AliasStorageInterface
   */
  protected $aliasStorage;

  /**
   * @var string
   */
  protected $defaultLangcode;

  /**
   * GnarlyAliasStorage constructor.
   *
   * @param \Drupal\Core\Path\AliasStorageInterface $aliasStorage
   * @param \Drupal\Core\Language\LanguageDefault $languageDefault
   */
  public function __construct(AliasStorageInterface $aliasStorage, LanguageDefault $languageDefault) {
    $this->aliasStorage = $aliasStorage;
    $this->defaultLangcode = $languageDefault->get()->getId();
  }

  /**
   * {@inheritdoc}
   */
  public function save($source, $alias, $langcode = LanguageInterface::LANGCODE_NOT_SPECIFIED, $pid = NULL) {
    if ($langcode === LanguageInterface::LANGCODE_NOT_SPECIFIED) {
      return $this->aliasStorage->save($source, $alias, $langcode, $pid);
    }
    // \Drupal\webform\Entity\Webform::updatePaths() calls a path alias save
    // both for the specific and the neutral alias so throwing an exception
    // would break webform. But it discards the return value so just not saving
    // is fine.
    #throw new \LogicException('URL aliases are language neutral. Yes, this is gnarly.');
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function load($conditions) {
    return $this->aliasStorage->load($conditions);
  }

  /**
   * {@inheritdoc}
   */
  public function delete($conditions) {
    return $this->aliasStorage->load($conditions);
  }

  /**
   * {@inheritdoc}
   */
  public function preloadPathAlias($preloaded, $langcode) {
    return $this->aliasStorage->preloadPathAlias($preloaded, $langcode);
  }

  /**
   * {@inheritdoc}
   */
  public function lookupPathAlias($path, $langcode) {
    return $this->aliasStorage->lookupPathAlias($path, $langcode);
  }

  /**
   * {@inheritdoc}
   */
  public function lookupPathSource($path, $langcode) {
    return $this->aliasStorage->lookupPathSource($path, $langcode);
  }

  /**
   * {@inheritdoc}
   */
  public function aliasExists($alias, $langcode, $source = NULL) {
    return $this->aliasStorage->aliasExists($alias, $langcode, $source);
  }

  /**
   * {@inheritdoc}
   */
  public function languageAliasExists() {
    return $this->aliasStorage->languageAliasExists();
  }

  /**
   * {@inheritdoc}
   */
  public function getAliasesForAdminListing($header, $keys = NULL) {
    return $this->aliasStorage->getAliasesForAdminListing($header, $keys);
  }

  /**
   * {@inheritdoc}
   */
  public function pathHasMatchingAlias($initial_substring) {
    return $this->aliasStorage->pathHasMatchingAlias($initial_substring);
  }

}
