<?php

namespace Drupal\Tests\gnarly\Kernel {

use Drupal\Core\Path\AliasStorage;
use Drupal\gnarly\Commands\GnarlyCommands;
use Drupal\KernelTests\KernelTestBase;
use Drupal\language\Entity\ConfigurableLanguage;

/**
 * @group gnarly
 */
class DrushGnarlyTest extends KernelTestBase {

  /**
   * @var array
   */
  protected $aliases = [];

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'system',
    'user',
    'views',
    'language',
    'udon',
    'gnarly',
    'link',
    'redirect'
  ];


  protected function setUp() {
    parent::setUp();
    $this->installConfig('system');
    $this->installSchema('system', 'sequences');
    $this->installEntitySchema('redirect');
    // Switch default language to French to ensure no 'en' is hardcoded.
    ConfigurableLanguage::createFromLangcode('fr')->save();
    $this->config('system.site')->set('default_langcode', 'fr')->save();
    // Create a second, non-default language as well.
    ConfigurableLanguage::createFromLangcode('de')->save();
    $db = \Drupal::database();
    $db->schema()->createTable(AliasStorage::TABLE, AliasStorage::schemaDefinition());
    $insert = $db->insert('url_alias')->fields(['source', 'alias', 'langcode']);
    foreach (['fr', 'de', 'und'] as $langcode) {
      $alias = [
        'source' => '/' . $this->randomMachineName(),
        'alias' => '/' . $this->randomMachineName(),
      ];
      $this->aliases[$alias['source']] = $alias;
      $insert->values($alias + ['langcode' => $langcode]);
    }
    $insert->execute();
  }

  public function testUpdateToNeutral() {
    (new GnarlyCommands())->updateToNeutral();
    $this->assertSame(3, count($this->aliases));
    $db = \Drupal::database();
    foreach ($db->query('SELECT source, alias, langcode FROM {url_alias}', [], ['fetch' => \PDO::FETCH_ASSOC]) as $row) {
      $this->assertSame('und', $row['langcode']);
      unset($row['langcode']);
      $this->assertSame($row, $this->aliases[$row['source']]);
      unset($this->aliases[$row['source']]);
    }
    $this->assertSame(1, count($this->aliases));
    $alias = reset($this->aliases);
    /** @var $repository \Drupal\redirect\RedirectRepository */
    $repository = \Drupal::service('redirect.repository');
    // The redirect is from the alias to the source.
    $redirect = $repository->findMatchingRedirect($alias['alias'], [], 'de');
    $this->assertSame('internal:' . $alias['source'], $redirect->getRedirect()['uri']);
    $this->assertSame(1, (int) $db->query('SELECT COUNT(*) FROM {redirect}')->fetchField());
  }

}
}

namespace Drupal\gnarly\Commands {

  function drush_backend_batch_process() {
    $batch =& batch_get();
    $batch['progressive'] = FALSE;
    batch_process();
  }

}
