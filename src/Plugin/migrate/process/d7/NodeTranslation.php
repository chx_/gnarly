<?php

namespace Drupal\gnarly\Plugin\migrate\process\d7;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Translated node path and language replacement.
 *
 * The url_alias core destination plugin does the same but this module needs to
 * do it in process because a) redirect needs the same path treatment b) the
 * langcode mapping from default to und needs to happen after langcode is
 * retrieved from node translation.
 *
 * @MigrateProcessPlugin(
 *   id = "gnarly_node_translation",
 * )
 */
class NodeTranslation extends ProcessPluginBase {

  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $source = $row->getDestinationProperty('source');
    if ($value && preg_match('#^/node/\d+$#', $source)) {
      list($nid, $langcode) = $value;
      $row->setDestinationProperty('source', "/node/$nid");
      $row->setDestinationProperty('langcode', $langcode);
    }
    // By returning nothing, node_translation will never get set and the core
    // destination plugin will not override the mapped langcode.
  }

}
