<?php

namespace Drupal\gnarly;

use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Path\AliasStorageInterface;
use Drupal\migrate\MigrateException;
use Drupal\migrate\Plugin\MigrateDestinationInterface;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\Row;
use Drupal\path\Plugin\migrate\destination\UrlAlias;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Migrate destination class saving aliases or redirects as appropriate.
 */
class GnarlyUrlAlias extends UrlAlias {

  /**
   * {@inheritdoc}
   */
  protected $supportsRollback = TRUE;

  /**
   * @var \Drupal\migrate\Plugin\MigrateDestinationInterface
   */
  protected $redirectDestination;

  /**
   * @var string
   */
  protected $defaultLangcode;


  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration, AliasStorageInterface $aliasStorage, MigrateDestinationInterface $redirectDestination, $defaultLangcode) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration, $aliasStorage);
    $this->redirectDestination = $redirectDestination;
    $this->defaultLangcode = $defaultLangcode;
  }

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration = NULL) {
    /** @var \Drupal\migrate\Plugin\MigrateDestinationInterface $redirectDestination */
    $redirectDestination = $container->get('plugin.manager.migration')
      ->createInstance('d7_path_redirect')
      ->getDestinationPlugin();
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $migration,
      $container->get('path.alias_storage'),
      $redirectDestination,
      $container->get('language.default')->get()->getId()
    );
  }

  public function import(Row $row, array $old_destination_id_values = []) {
    // self::migrationPluginsAlter() sets this property.
    $type = $row->getDestinationProperty('gnarly_type');
    if ($old_destination_id_values) {
      list($id, $oldType) = $old_destination_id_values;
      if (isset($oldType) && $oldType !== $type) {
        throw new MigrateException('Changing the langcode to specific and back after migration is not allowed. Yes, this is gnarly.');
      }
      $old_destination_id_values = [$id];
    }
    if ($type === 'url_alias') {
      $ids = parent::import($row, $old_destination_id_values);
    }
    else {
      // self::migrationPluginsAlter() already prepared all the values for the
      // redirect.
      $ids = $this->redirectDestination->import($row, $old_destination_id_values);
    }
    $ids[] = $type;
    return $ids;
  }

  public function rollback(array $destination_identifier) {
    list($id, $type) = $destination_identifier;
    if ($type === 'url_alias') {
      $this->aliasStorage->delete(['pid' => $id]);
    }
    else {
      $this->redirectDestination->rollback([$id]);
    }
  }

  public function getIds() {
    $ids = parent::getIds();
    $ids['type']['type'] = 'string';
    return $ids;
  }

  public static function migrationPluginsAlter(array &$migration) {
    if ($migration['destination']['plugin'] === 'url_alias') {
      $migration['process']['_langcode'] = $migration['process']['langcode'];
      // The new definition of langcode refers to @_langcode so the definition
      // of _langcode needs to come before langcode but since langcode is
      // defined in the YAML, it'll come before everything defined in this
      // alter, remove it before redefining it.
      unset($migration['process']['langcode']);
      // Override source and langcode with values from node_translation.
      $migration['process']['node_translation'][] = [
        'plugin' => 'gnarly_node_translation',
      ];
      $migration['process']['langcode'] = [
        'plugin' => 'static_map',
        'source' => '@_langcode',
        'map' => [
          \Drupal::languageManager()->getDefaultLanguage()->getId() => LanguageInterface::LANGCODE_NOT_SPECIFIED,
        ],
        'bypass' => TRUE,
      ];
      // Default (already mapped) and language neutral aliases stay aliases,
      // the rest become redirects.
      $migration['process']['gnarly_type'] = [
        'plugin' => 'static_map',
        'source' => '@langcode',
        'map' => [
          LanguageInterface::LANGCODE_NOT_SPECIFIED => 'url_alias',
        ],
        'default_value' => 'redirect',
      ];
      // Prepare for the redirect entity.
      $migration['process']['language'] = '@langcode';
      // Remove the leading slash to pretend it is a D7 path.
      $migration['process']['redirect_source/path'] = [
        'plugin' => 'substr',
        'source' => '@alias',
        'start' => 1,
      ];
      // Same for the source. d7_path_redirect expects an array of
      // [path, query] and creating an array inside a process pipeline without a
      // custom plugin or callback is not possible, it's much simpler to store
      // it in a destination property which won't be persisted.
      $migration['process']['_uri_source'] = [
        'plugin' => 'substr',
        'source' => '@source',
        'start' => 1,
      ];
      $migration['process']['redirect_redirect/uri'] = [
        // First, no need to route if it won't become a redirect.
        [
          'plugin' => 'static_map',
          'source' => '@gnarly_type',
          'map' => [
            'url_alias' => 0,
            'redirect' => 1,
          ],
        ],
        [
          'plugin' => 'skip_on_empty',
          'method' => 'process',
        ],
        // Second, run the routing logic in d7_path_redirect.
        [
          'plugin' => 'd7_path_redirect',
          'source' => ['@_uri_source'],
        ],
      ];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function fields(MigrationInterface $migration = NULL) {
    return parent::fields($migration) + [
      'type' => 'URL alias or redirect',
    ];
  }

}
