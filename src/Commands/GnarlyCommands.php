<?php

namespace Drupal\gnarly\Commands;

use Drupal\udon\Commands\UdonCommands;
use Drush\Commands\DrushCommands;

class GnarlyCommands extends DrushCommands {

  /**
   * Update aliases to neutral.
   *
   * @command gnarly_update_to_neutral
   */
  public function updateToNeutral() {
    if (!class_exists('Drupal\udon\Commands\UdonCommands')) {
      throw new \LogicException('The udon module is required for this command.');
    }
    $batch['operations'] = [
      [[UdonCommands::class, 'run'], ['gnarly', 8001]],
      [[UdonCommands::class, 'run'], ['gnarly', 8002]],
      ['drupal_flush_all_caches', []]
    ];
    batch_set($batch);
    drush_backend_batch_process();
  }

}
