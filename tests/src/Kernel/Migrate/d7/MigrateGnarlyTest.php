<?php

namespace Drupal\Tests\gnarly\Kernel\Migrate\d7;

use Drupal\Tests\path\Kernel\Migrate\d7\MigrateUrlAliasTest;

/**
 * Tests the migration of aliases from Drupal 7.
 *
 * @group gnarly
 * @group migrate_drupal_7
 */
class MigrateGnarlyTest extends MigrateUrlAliasTest {

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'gnarly',
    'link',
    'redirect'
  ];

  protected function installEntitySchema($entity_type_id) {
    parent::installEntitySchema($entity_type_id);
    // redirect needs to be installed before parent::setUp runs the migration
    // so to avoid copying the entire setUp method, just sneak in redirect here.
    if ($entity_type_id === 'node') {
      parent::installEntitySchema('redirect');
    }
  }

  // testUrlAlias() does not need any changes.

  /**
   * Test the gnarly URL alias migration with translated nodes.
   */
  public function testUrlAliasWithTranslatedNodes() {
    $aliasStorage = $this->container->get('path.alias_storage');

    // Alias for the 'The thing about Deep Space 9' node in English.
    $path = $aliasStorage->load(['alias' => '/deep-space-9']);
    $this->assertSame('/node/2', $path['source']);
    $this->assertSame('und', $path['langcode']);

    // Alias for the 'The thing about Deep Space 9' Icelandic translation,
    // which should now redirect to node/2 instead of node/3.
    $this->assertSame(FALSE, $aliasStorage->load(['alias' => '/deep-space-9-is']));
    $this->assertSame(2, $this->getRedirectNid('deep-space-9-is'));

    // Alias for the 'The thing about Firefly' node in Icelandic is now a
    // redirect.
    $path = $aliasStorage->load(['alias' => '/firefly-is']);
    $this->assertSame(FALSE, $path);
    $this->assertSame(4, $this->getRedirectNid('firefly-is'));

    // Alias for the 'The thing about Firefly' English translation,
    // which should now point to node/4 instead of node/5.
    $path = $aliasStorage->load(['alias' => '/firefly']);
    $this->assertSame('/node/4', $path['source']);
    $this->assertSame('und', $path['langcode']);
  }

  public function testDatabaseTables() {
    $db = \Drupal::database();
    $this->assertEmpty($db->query('SELECT COUNT(*) FROM {url_alias} WHERE langcode != :und', [':und' => 'und'])->fetchField());
    $map['url_alias'] = $db->query('SELECT pid, alias FROM {url_alias}')->fetchAllKeyed();
    $map['redirect'] = $db->query('SELECT rid, redirect_source__path FROM {redirect}')->fetchAllKeyed();
    foreach ($db->query('SELECT destid1, destid2 FROM {migrate_map_d7_url_alias}') as $row) {
      $actuals[$row->destid2][] = $map[$row->destid2][$row->destid1];
    }
    sort($actuals['redirect']);
    sort($actuals['url_alias']);
    $expected = [
      'redirect' => ['deep-space-9-is', 'firefly-is'],
      'url_alias' => ['/deep-space-9', '/firefly', '/source-noslash', '/term33'],
    ];
    $this->assertSame($expected, $actuals);
  }

  /**
   * @param string $source_path
   *
   * @return int
   */
  protected function getRedirectNid(string $source_path) {
    $redirect = $this->container->get('redirect.repository')
      ->findMatchingRedirect($source_path, [], 'is')
      ->getRedirect();
    // Contrib redirect uses internal:/node/4, we run a fork which uses
    // entity:node/4.
    preg_match('#^(?:internal:/|entity:)node/(\d+)$#', $redirect['uri'], $matches);
    return (int) $matches[1];
  }

}
