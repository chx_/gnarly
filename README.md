## Why is this module necessary?

As the language_neutral_alias project README notes, users might expect
language aliases to be language neutral or in Drupal core parlance: not
specified (I am not specified either, so I sympathize). I'd add the
interaction between redirect, i18n and url aliases is, well, ahem, gnarly.

## What's the problem with language_neutral_alias?

This module is a different approach to the same problem. While aliases are
neutral, redirects can still be language specific and the module indeed uses
those. language_neutral_alias changes the langcode passed to the path alias
storage to be neutral, this module does not change the langcode, intead it
does not save anything but the neutral aliases. It changes the API consumers
instead:

1. Path fields on entities become non-translateable.
2. The path admin form no longer offers a language selector.
3. Migrations with an url_alias destination turns the default language into
language neutral and saves redirects for non-default languages.

While obviously not all of contrib could be reviewed, I only found one module
which uses the alias storage: webform. It always saves a specific and a
neutral alias so this module will neither break webform nor is a change to
webform necessary. It is truly an awesome module (in other words, totally not 
gnarly).

## What happens to existing aliases

Run drush gnarly_update_to_neutral after install.
