<?php

namespace Drupal\Tests\gnarly\Functional;

use Drupal\Core\Language\LanguageInterface;
use Drupal\Tests\BrowserTestBase;

/**
 * @group gnarly
 */
class GnarlyUiTest extends BrowserTestBase {

  public static $modules = ['gnarly', 'node', 'language', 'path', 'content_translation'];

  protected function setUp() {
    parent::setUp();
    $this->drupalCreateContentType(['type' => 'page', 'name' => 'Basic page']);

    // Create and log in user.
    $web_user = $this->drupalCreateUser(['administer content types', 'edit own page content', 'create page content', 'administer url aliases', 'create url aliases', 'administer languages', 'access administration pages', 'translate any entity']);
    $this->drupalLogin($web_user);

    // Enable French language.
    $edit = ['predefined_langcode' => 'fr'];
    $this->drupalPostForm('admin/config/regional/language/add', $edit, t('Add language'));

    // Enable URL language detection and selection.
    $edit = ['language_interface[enabled][language-url]' => 1];
    $this->drupalPostForm('admin/config/regional/language/detection', $edit, t('Save settings'));
  }

  /**
   * Tests the path admin UI.
   */
  public function testPathAdminUi() {
    $assertSession = $this->assertSession();
    $name = $this->randomMachineName(8);
    $edit = [];
    $edit['source'] = '/admin/config/search/path';
    $edit['alias'] = '/' . $name;
    $assertSession->fieldNotExists('langcode');
    $this->drupalPostForm('admin/config/search/path/add', $edit, t('Save'));

    $this->drupalGet($name);
    $assertSession->responseContains('Filter aliases');
    $this->drupalGet("fr/$name");
    $assertSession->responseContains('Filter aliases');

    $record = \Drupal::service('path.alias_storage')->load([
      'alias' => $edit['alias']
    ]);
    $this->assertSame(LanguageInterface::LANGCODE_NOT_SPECIFIED, $record['langcode']);
  }


  /**
   * Tests the node UI.
   */
  public function testNodeAddUi() {
    $alias = $this->randomMachineName(8);
    $title = $this->randomString();
    $edit = [
      'title[0][value]' => $title,
      'path[0][alias]' => '/' . $alias,
    ];
    $this->drupalPostForm('node/add/page', $edit, t('Save'));
    $nid = $this->drupalGetNodeByTitle($title)->id();
    $title = htmlentities($title, ENT_QUOTES);
    $this->drupalGet($alias);
    $this->assertSession()->responseContains($title);
    $this->drupalGet("fr/$alias");
    $this->assertSession()->responseContains($title);

    $this->drupalGet("node/$nid/translations/add/en/fr");
    $this->assertSession()->responseContains($alias);

    $titleFr = $this->randomString();
    $aliasFr = $this->randomMachineName(8);
    $edit = [
      'title[0][value]' => $titleFr,
      'path[0][alias]' => '/' . $aliasFr,
    ];
    $this->drupalPostForm(NULL, $edit, t('Save'));
    $titleFr = htmlentities($titleFr, ENT_QUOTES);

    $this->drupalGet("node/$nid/edit");
    $this->assertSession()->responseContains($title);
    $this->assertSession()->responseContains($alias);
    $this->assertSession()->responseNotContains($aliasFr);
    $this->drupalGet("fr/node/$nid/edit");
    $this->assertSession()->responseContains($titleFr);
    $this->assertSession()->responseContains($alias);
    $this->assertSession()->responseNotContains($aliasFr);
    $this->drupalGet($aliasFr);
    $this->assertSession()->statusCodeEquals(404);
    $this->drupalGet("fr/$aliasFr ");
    $this->assertSession()->statusCodeEquals(404);
    $this->drupalGet($alias);
    $this->assertSession()->responseContains($title);
    $this->drupalGet("fr/$alias");
    $this->assertSession()->responseContains($titleFr);
  }

}
